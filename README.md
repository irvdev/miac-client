It's easy

##Quick start

* `npm i`
* Запуск dev сервера - `gulp`
* Сборка - `gulp prod` 

##Directory Layout

	interact                 # Project root
	├── /src/                # Source files
	├── /dev/                # Compiled files for developing
	├── /dist/               # Minified files for production
