
// PRELOADER
$(window).on('load', function () {
  $('.a-l-preloader').fadeOut('slow');
});
// PRELOADER

// INIT SIDENAV
$(document).ready(function() {
  $(".sidenav").sidenav();
})
// INIT SIDENAV

// INIT COLLAPSIBLE
$(document).ready(function() {
  $('.collapsible').collapsible();
})
// INIT COLLAPSIBLE

// INIT MATCHHEIGHT
$(document).ready(function() {
  $(function() {
    $('.c-e-default').matchHeight({});
  });
})
// INIT MATCHHEIGHT

// INIT FANCYBOX
$(document).ready(function() {
  $('.c-e-photo-fancybox a').fancybox({ protect: true })
})
// INIT FANCYBOX

// INIT CARDTABLE
$(document).ready(function() {
  $('table').cardtable();
});
// INIT CARDTABLE

// INIT SlIDER
$(document).ready(function() {
  $('.c-b-slider').slick({
    dots: true,
    autoplay: true,
    fade: true,
    prevArrow: '<button type="button" class="slick-left"><i class="fa fa-caret-left"></i></button>',
    nextArrow: '<button type="button" class="slick-right"><i class="fa fa-caret-right"></i></button>'
  });
})
// INIT SlIDER

// INIT ELEMENTS SLIDERS
$(document).ready(function() {
  $('.c-b-elements-slider.col4 .c-b-elements-slider__carousel').slick({
    dots: true,
    autoplay: true,
    prevArrow: '<button type="button" class="slick-left"><i class="fa fa-caret-left"></i></button>',
    nextArrow: '<button type="button" class="slick-right"><i class="fa fa-caret-right"></i></button>',
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.c-b-elements-slider.col3 .c-b-elements-slider__carousel').slick({
    dots: true,
    autoplay: true,
    prevArrow: '<button type="button" class="slick-left"><i class="fa fa-caret-left"></i></button>',
    nextArrow: '<button type="button" class="slick-right"><i class="fa fa-caret-right"></i></button>',
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.c-b-elements-slider.col1 .c-b-elements-slider__carousel').slick({
    dots: true,
    autoplay: true,
    prevArrow: '<button type="button" class="slick-left"><i class="fa fa-caret-left"></i></button>',
    nextArrow: '<button type="button" class="slick-right"><i class="fa fa-caret-right"></i></button>',
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1
  });
});
// INIT ELEMENTS SLIDERS  

// INIT DATEPICKER
$(document).ready(function() {
  $('.datepicker').datepicker({
    format: 'dd.mm.yyyy',
    autoClose: true,
    i18n: {
      months: [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь'
      ],
      monthsShort: [
        'Янв',
        'Фев',
        'Мар',
        'Апр',
        'Май',
        'Июн',
        'Июл',
        'Авг',
        'Сен',
        'Окт',
        'Ноя',
        'Дек'
      ],
      weekdays: [
        'Воскресенье',
        'Понедельник',
        'Вторник',
        'Среда',
        'Четверг',
        'Пятница',
        'Суббота'
      ],
      weekdaysShort: [
        'Вс',
        'Пн',
        'Вт',
        'Ср',
        'Чт',
        'Пт',
        'Сб'
      ],
      weekdaysAbbrev: ['В','П','В','С','Ч','П','С'],
      cancel: 'Отмена',
      clear: 'Очистить',
      done: 'Ok'
    }
  });
});
// INIT DATEPICKER

// INIT SELECT
$(document).ready(function(){
  $('select').formSelect();
});
// INIT SELECT

// INIT TABS
$(document).ready(function(){
  $('.tabs').tabs();
});
// INIT TABS

// SEARCH DIAPAZON
$(document).ready(function(){
  $('#searchDateDiapazonGo').click(function() {
    $('#searchDateDiapazonInputs').show()
    $('#searchDateDiapazonGo').hide()
  });
});
// SEARCH DIAPAZON

// MAIN-NAV
$(document).ready(function() {
  
  var isOpenMainNav = false
  
  $('.trigger2level').click(function(e) {
    
    if ($(e.target).closest('li').hasClass('collapse')) {
      $('.trigger2level').closest('li').removeClass('collapse')
      $('.trigger3level').closest('li').removeClass('collapse')
      isOpenMainNav = false
      return 
    }
    
    $('.trigger3level').closest('li').removeClass('collapse')
    $(e.target).closest('li').siblings('li').removeClass('collapse')

    var submenuPosition = $(e.target).offset().left
    var windowWidth = $(window).width()
    var submenu3levelPosition = (submenuPosition + 300 - windowWidth) * - 1 + 'px'

    if (submenuPosition + 300 > windowWidth)
      $(e.target).siblings('.a-b-main-nav__submenu').css({ left: submenu3levelPosition })
    else
      $(e.target).siblings('.a-b-main-nav__submenu').css({ left: "-1px" })

    $(e.target).closest('li').addClass('collapse')
    isOpenMainNav = true
  })
  
  $('.trigger3level').click(function(e) {
    if ($(e.target).closest('li').hasClass('collapse')) {
      $(e.target).closest('li').removeClass('collapse')
      return 
    }

    
    $(e.target).closest('li').siblings('li').removeClass('collapse')
    
    var submenuPosition = $(e.target).offset().left
    var windowWidth = $(window).width()

    if (submenuPosition + 600 > windowWidth)
      $(e.target).siblings('.a-b-main-nav__submenu').css({ left: "calc(-100% + 1px)" })
    else
      $(e.target).siblings('.a-b-main-nav__submenu').css({ left: "calc(100% - 1px)" })

    $(e.target).closest('li').addClass('collapse')
  })

  $(document).click(function (e) {
    if (isOpenMainNav) {
      if (!$(e.target).closest('.a-b-main-nav__submenu, .a-b-main-nav__submenu-link, .trigger2level, .trigger3level').length) {
        $('.trigger2level, .trigger3level').parent('li').removeClass('collapse')
        isOpenMainNav = false
      }
    }
  })
})
// MAIN-NAV

// SIDEBAR MAIN-NAV
$(document).ready(function() {
  navSubPosition();

  var isOpenNavSub = false;

  $('.a-b-sb-main-nav__item-a.open-nav-sub').click(function (e) {
    if (isOpenNavSub) {
      $('.nav-sub').removeClass('animated fadeInRight').addClass('animated fadeOutRight');
      $('.a-b-sb-main-nav__item-a.open-nav-sub').removeClass('active')
      setTimeout(function() {
        $('.nav-sub').hide()
        $(e.target).addClass('active')
        $(e.target).siblings().removeClass('animated fadeOutRight').addClass('animated fadeInRight').show();
      }, 400);
      isOpenNavSub = true;
    } else {
      $(e.target).addClass('active')
      $(e.target).siblings().removeClass('animated fadeOutRight').addClass('animated fadeInRight').show();
      isOpenNavSub = true;
    }
  });

  $('.nav-sub__close').click(function() {
    if (isOpenNavSub) {
      $('.nav-sub').removeClass('animated fadeInRight').addClass('animated fadeOutRight');
      $('.a-b-sb-main-nav__item-a.open-nav-sub').removeClass('active')
      setTimeout(function() {
        $('.nav-sub').hide()
      }, 400);
      isOpenNavSub = false;
    }
  });

  $(document).click(function (e) {
    if (isOpenNavSub) {
      if (!$(e.target).closest('.nav-sub, .a-b-sb-main-nav__item-a').length) {
        $('.nav-sub').removeClass('animated fadeInRight').addClass('animated fadeOutRight');
        $('.a-b-sb-main-nav__item-a.open-nav-sub').removeClass('active')
        setTimeout(function() {
          $('.nav-sub').hide()
        }, 400);
        isOpenNavSub = false;
      }
    }
  });
})

$(window).resize(function() {
  navSubPosition();
  $('.c-b-elements-list .c-e-default').matchHeight({});
  $('.c-b-elements-slider .c-e-default').matchHeight({});
});

function navSubPosition() {
  var navSubWidth = $('.c-l-main').outerWidth() - $('.c-l-main__sb-l-wrap').outerWidth() - 60;
  var navSubRight = (navSubWidth + 30) * -1;
  $('.nav-sub').css({'width': navSubWidth, 'right': navSubRight});
}
// SIDEBAR MAIN-NAV