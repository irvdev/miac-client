// VISUALLY

$(document).ready(function() {

  // INIT VISUALL MODE
  var visuallyMode = localStorage.getItem('visuallyMode')
  var visuallySize = localStorage.getItem('visuallySize')
  var visuallySpacing = localStorage.getItem('visuallySpacing')
  
  if (visuallyMode) {
  
    VisuallyModeGo()

    if (visuallyMode !== 'black-on-white') {

      VisuallySchemaReset()
      $('body').addClass(visuallyMode)

    }

    if (visuallySize) {

      $('body').css({ 'fontSize': visuallySize })

      $('.visually-size').removeClass('active')
      if (visuallySize === '15px') $('.visually-size-x1').addClass('active')
      if (visuallySize === '20px') $('.visually-size-x2').addClass('active')
      if (visuallySize === '24px') $('.visually-size-x3').addClass('active')

    }

    if (visuallySpacing) {

      $('body').css({ 'letterSpacing': visuallySpacing })

      $('.visually-spacing').removeClass('active')
      if (visuallySpacing === 'normal') $('.visually-spacing-1').addClass('active')
      if (visuallySpacing === '2px') $('.visually-spacing-2').addClass('active')
      if (visuallySpacing === '3px') $('.visually-spacing-3').addClass('active')

    }

    $(function() {
      $('.c-e-default').matchHeight._update({});
    });
    
  }
  // INIT VISUALL MODE  
  
  $('.a-b-visually__go').click(function() {

    VisuallyModeGo()

    localStorage.setItem('visuallyMode', 'black-on-white')

  })

  $('.a-b-visually__end').click(function() {

    VisuallyModeEnd()

  })

  $('.a-b-visually-modal__set-img-switch input').change(function() {

    if(this.checked) {

      $('body').removeClass('no-img')
      
    } else {
      
      $('body').addClass('no-img')

    }

  })

  $('.visually-size-x1').click(function() {

    $('.visually-size').removeClass('active')
    $('.visually-size-x1').addClass('active')

    $('body').css({ 'fontSize': '15px' })
    localStorage.setItem('visuallySize', '15px')

    $(function() {
      $('.c-e-default').matchHeight._update({});
    });

  })

  $('.visually-size-x2').click(function() {

    $('.visually-size').removeClass('active')
    $('.visually-size-x2').addClass('active')

    $('body').css({ 'fontSize': '20px' })
    localStorage.setItem('visuallySize', '20px')

    $(function() {
      $('.c-e-default').matchHeight._update({});
    });

  })

  $('.visually-size-x3').click(function() {

    $('.visually-size').removeClass('active')
    $('.visually-size-x3').addClass('active')

    $('body').css({ 'fontSize': '24px' })
    localStorage.setItem('visuallySize', '24px')

    $(function() {
      $('.c-e-default').matchHeight._update({});
    });

  })

  $('.visually-spacing-1').click(function() {

    $('.visually-spacing').removeClass('active')
    $('.visually-spacing-1').addClass('active')

    $('body').css({ 'letterSpacing': 'normal' })
    localStorage.setItem('visuallySpacing', 'normal')

    $(function() {
      $('.c-e-default').matchHeight._update({});
    });

  })

  $('.visually-spacing-2').click(function() {

    $('.visually-spacing').removeClass('active')
    $('.visually-spacing-2').addClass('active')

    $('body').css({ 'letterSpacing': '2px' })
    localStorage.setItem('visuallySpacing', '2px')

    $(function() {
      $('.c-e-default').matchHeight._update({});
    });

  })

  $('.visually-spacing-3').click(function() {

    $('.visually-spacing').removeClass('active')
    $('.visually-spacing-3').addClass('active')

    $('body').css({ 'letterSpacing': '3px' })
    localStorage.setItem('visuallySpacing', '3px')

    $(function() {
      $('.c-e-default').matchHeight._update({});
    });

  })
  
  $('.visually-black-on-white').click(function() {

    VisuallySchemaReset()
    $('body').addClass('black-on-white')

    localStorage.setItem('visuallyMode', 'black-on-white')

  })

  $('.visually-white-on-black').click(function() {

    VisuallySchemaReset()
    $('body').addClass('white-on-black')

    localStorage.setItem('visuallyMode', 'white-on-black')

  })

  $('.visually-blue-on-azure').click(function() {

    VisuallySchemaReset()
    $('body').addClass('blue-on-azure')

    localStorage.setItem('visuallyMode', 'blue-on-azure')

  })

  $('.visually-brown-on-beige').click(function() {

    VisuallySchemaReset()
    $('body').addClass('brown-on-beige')

    localStorage.setItem('visuallyMode', 'brown-on-beige')

  })

  $('.visually-green-on-brown').click(function() {

    VisuallySchemaReset()
    $('body').addClass('green-on-brown')

    localStorage.setItem('visuallyMode', 'green-on-brown')

  })

})

function VisuallySchemaReset() {

  $('body').removeClass('black-on-white')
  $('body').removeClass('white-on-black')
  $('body').removeClass('blue-on-azure')
  $('body').removeClass('brown-on-beige')
  $('body').removeClass('green-on-brown')

}

function VisuallyModeGo() {

  $('.a-b-visually__go').hide()
  $('.a-b-visually__end').css({ 'display': 'inline-block' })
  $('.a-b-visually__settings').css({ 'display': 'inline-block' })

  $('body').addClass('black-on-white')
  
}

function VisuallyModeEnd() {
  
  $('.a-b-visually__go').css({ 'display': 'inline-block' })
  $('.a-b-visually__end').hide()
  $('.a-b-visually__settings').hide()

  $('body').css({ 'fontSize': '15px' })
  $('body').css({ 'letterSpacing': 'normal' })

  $('body').removeClass('no-img')
  
  VisuallySchemaReset()

  localStorage.removeItem('visuallyMode')
  localStorage.removeItem('visuallySize')
  localStorage.removeItem('visuallySpacing')

  $('.visually-size').removeClass('active')
  $('.visually-size-x1').addClass('active')


  $('.visually-spacing').removeClass('active')
  $('.visually-spacing-1').addClass('active')

  $(function() {
    $('.c-e-default').matchHeight._update({});
  });
  
}

// VISUALLY