// Gulp 4
var gulp = require('gulp')
var pug = require('gulp-pug')
var sass = require('gulp-sass')
var concat = require('gulp-concat')
var plumber = require('gulp-plumber')
var prefix = require('gulp-autoprefixer')
var imagemin = require('gulp-imagemin')
var browserSync = require('browser-sync')

var useref = require('gulp-useref')
var gulpif = require('gulp-if')
var cssmin = require('gulp-clean-css')
var uglify = require('gulp-uglify')
var rimraf = require('rimraf')

var paths = {
	src: 'src/',
	devDir: 'dev/',
	outputDir: './dist/'
}

// ws
gulp.task('ws', function(cb) {
  browserSync({
    server: {
      baseDir: 'dev'
    },
    port: 4000,
    notify: false,
    open: false
  }, cb)
})

// clean
gulp.task('clean', function(cb) {
	rimraf(paths.outputDir, cb)
})

// watch
gulp.task('watch', function() {
	gulp.watch(paths.src + '**/*.pug', gulp.series('pug'))
	gulp.watch(paths.src + '**/*.sass', gulp.series('sass'))
	gulp.watch(paths.src + '**/*.js', gulp.series('scripts'))
	gulp.watch(paths.src + '_assets/img/**/*.*', gulp.series('imgBuild'))
	gulp.watch(paths.src + '_assets/libs/**/*.*', gulp.series('copyLibs'))
})

// pug compile
gulp.task('pug', function() {
	return gulp.src([paths.src + 'pages/*.pug'])
		.pipe(plumber())
		.pipe(pug({pretty: true}))
		.pipe(gulp.dest(paths.devDir))
})

//copy img to devDir
gulp.task('copyImg', function() {
	return gulp.src(paths.src + '_assets/img/**/*')
		.pipe(gulp.dest(paths.devDir + 'img/'));
})

// sass compile
gulp.task('sass', function() {
	return gulp.src(paths.src + 'main.sass')
		.pipe(plumber())
		.pipe(sass().on('error', sass.logError))
		.pipe(prefix({
			browsers: ['last 10 versions'],
			cascade: true
		}))
		.pipe(gulp.dest(paths.devDir + 'css/'))
		.pipe(browserSync.stream())
})

// js compile
gulp.task('scripts', function() {
	return gulp.src([
			paths.src + '**/*.js',
			'!' + paths.src + '_assets/**/*.js'
		])
		.pipe(concat('main.js'))
		.pipe(gulp.dest(paths.devDir + 'js/'))
		.pipe(browserSync.stream())
})

// copy libs to devDir
gulp.task('copyLibs', function() {
	return gulp.src(paths.src + '_assets/libs/**/*')
		.pipe(gulp.dest(paths.devDir + 'libs/'))
})

// copy images to outputDir
gulp.task('imgBuild', gulp.series(function() { 
	return gulp.src(paths.devDir + 'img/**/*.*')
		.pipe(imagemin())
		.pipe(gulp.dest(paths.outputDir + 'img/'))
}))

// copy stuff to outputDir
gulp.task('prodBuild', gulp.series(function() { 
	return gulp.src(paths.devDir + '*.html')
		.pipe( useref() )
		.pipe( gulpif('*.js', uglify()) )
		.pipe( gulpif('*.css', cssmin()) )
		.pipe( gulp.dest(paths.outputDir) )
}))

//copy fonts to outputDir
gulp.task('font-awesome-fonts', gulp.series(function() { 
	return gulp.src(paths.devDir + 'libs/font-awesome/webfonts/**/*')
		.pipe(gulp.dest(paths.outputDir + 'webfonts/'))
}))
gulp.task('materialize-fonts', gulp.series(function() { 
	return gulp.src(paths.devDir + 'libs/materialize/fonts/**/*')
		.pipe(gulp.dest(paths.outputDir + 'fonts/'))
}))
gulp.task('materialicons', gulp.series(function() { 
	return gulp.src(paths.devDir + 'libs/materialicons/fonts/**/*')
		.pipe(gulp.dest(paths.outputDir + 'css/fonts/'))
}))
gulp.task('slick-fonts', gulp.series(function() { 
	return gulp.src(paths.devDir + 'libs/slick/fonts/**/*')
		.pipe(gulp.dest(paths.outputDir + 'css/fonts/'))
}))
gulp.task('slick-loader', gulp.series(function() { 
	return gulp.src(paths.devDir + 'libs/slick/ajax-loader.gif')
		.pipe(gulp.dest(paths.outputDir + 'css/'))
}))
gulp.task('fontsBuild', gulp.series(
	'font-awesome-fonts',
	'materialize-fonts',
	'materialicons',
	'slick-fonts',
	'slick-loader'
))

gulp.task('devBuild', gulp.series('pug', 'sass', 'scripts', 'copyLibs', 'copyImg'))

gulp.task('default', gulp.series('devBuild', 'ws', 'watch'))

gulp.task('prod', gulp.series('clean', 'devBuild', 'fontsBuild', 'imgBuild', 'prodBuild'))
















// // Shared tasks
// gulp.task('glob', function () {
//   var pattern = '.build/**/*.css'

//   gulp.src(pattern, { read:false })
//     .pipe(using())
// })

// gulp.task(function (cb) {
//   del(paths.dirs.build, cb)
// })


// // Development build specific tasks
// gulp.task('html', function () {
//   return gulp.src(paths.html)
//     .pipe(gulp.dest(paths.dirs.build))
// })

// gulp.task('json', function () {
//   return gulp.src(paths.json)
//     .pipe(gulp.dest(paths.dirs.build))
// })

// gulp.task('coffee', function () {
//   return gulp.src(paths.coffee)
//     .pipe(coffeelint())
//     .pipe(coffeelint.reporter())
//     .pipe(sourcemaps.init())
//     .pipe(coffee({ bare: true }))
//     .pipe(ngAnnotate( {
//       remove: true,
//       add: true,
//       single_quotes: true
//     }))
//     .pipe(sourcemaps.write('.', { sourceRoot: '/' }))
//     .pipe(gulp.dest(paths.dirs.build))
// })

// gulp.task('images', function () {
//   return gulp.src(paths.images)
//     .pipe(gulp.dest(paths.dirs.build))
// })

// gulp.task('sass', function () {
//   return gulp.src(paths.sass)
//     .pipe(using({ prefix: 'After changed:' }))
//     .pipe(sourcemaps.init())
//     .pipe(sass())
//     .pipe(changed(paths.dirs.build))
//     .pipe(sourcemaps.write('.', { sourceRoot: '/' }))
//     .pipe(gulp.dest(paths.dirs.build))
//     .pipe(grep('**/*.css', { read: false, dot: true }))
//     .pipe(browserSync.reload({stream:true}))
// })

// gulp.task('less', function () {
//   return gulp.src(paths.less)
//     .pipe(sourcemaps.init())
//     .pipe(less())
//     .pipe(changed(paths.dirs.build))
//     .pipe(sourcemaps.write('.', { sourceRoot: '/' }))
//     .pipe(gulp.dest(paths.dirs.build))
//     .pipe(grep('**/*.css', { read: false, dot: true }))
//     .pipe(browserSync.reload({stream:true}))
// })

// gulp.task('app', gulp.parallel('html', 'coffee', 'json', 'images', 'sass', 'less'))

// gulp.task('vendor:components:js', function () {
//   return gulp.src(paths.vendor.components.js)
//     .pipe(gulp.dest(paths.dirs.build))
// })

// gulp.task('vendor:components:other', function () {
//   return gulp.src(paths.vendor.components.nonJs)
//     .pipe(gulp.dest(paths.dirs.build))
// })

// gulp.task('vendor:components', gulp.parallel(
//   'vendor:components:js', 'vendor:components:other'
// ))

// gulp.task('vendor:js', function () {
//   return gulp.src(paths.vendor.bower.js)
//     .pipe(gulp.dest(paths.dirs.build + '/vendor'))
// })

// gulp.task('vendor:css', function () {
//   return gulp.src(paths.vendor.bower.css)
//     .pipe(gulp.dest(paths.dirs.build + '/vendor'))
// })

// gulp.task('vendor:fonts', function () {
//   return gulp.src(paths.vendor.bower.fonts, { base: 'bower_components' })
//     .pipe(gulp.dest(paths.dirs.build + '/vendor'))
// })

// gulp.task('vendor', gulp.parallel(
//   'vendor:components', 'vendor:js', 'vendor:css', 'vendor:fonts'
// ))


// gulp.task('all', gulp.parallel('app', 'vendor'))
// gulp.task('build', gulp.series('clean', 'all'))


// gulp.task('unit', function () {
//   karma.start({
//     configFile: __dirname + '/config/karma.js',
//     singleRun: true
//   })
// })

// gulp.task('webdriver-update', webdriver_update)
// gulp.task('selenium', gulp.series('webdriver-update', webdriver_standalone))
// gulp.task('e2e', function() {
//   gulp.src(paths.e2e)
//     .pipe((protractor({
//       configFile: 'config/protractor.js'
//     })).on('error', function(e) {
//       throw e
//     }))
//     .pipe(exit())
// })

// gulp.task('watch:styles', function () {
//   gulp.watch(paths.sass, 'sass')
//   gulp.watch(paths.less, 'less')
// })

// gulp.task('watch:code', function () {
//   gulp.watch([
//     paths.html,
//     paths.coffee,
//     paths.images,
//     paths.json,
//     paths.vendor.components.all,
//     paths.vendor.bower.js
//   ], gulp.series('build', browserSync.reload))
// })

// gulp.task('watch:unit', function () {
//   karma.start({
//     configFile: __dirname + '/config/karma.js',
//     autoWatch: true
//   })
// })

// gulp.task('watch', gulp.parallel('watch:code', 'watch:styles', 'watch:unit'))

// // Production build specific tasks
// gulp.task('html:prod', function () {
//   return gulp.src(paths.html)
//     .pipe(replaceHtml({
//       'css': 'all.min.css',
//       'js': 'all.min.js'
//     }))
//     .pipe(gulp.dest(paths.dirs.build))
// })

// gulp.task('json:prod', function () {
//   return gulp.src(paths.json)
//     .pipe(minifyJson())
//     .pipe(gulp.dest(paths.dirs.build))
// })

// gulp.task('images:prod', function () {
//   return gulp.src(paths.images)
//     .pipe(minifyImg())
//     .pipe(gulp.dest(paths.dirs.build))
// })

// gulp.task('fonts:prod', function () {
//   return gulp.src(paths.vendor.bower.fonts)
//     .pipe(gulp.dest(paths.dirs.build+'/fonts'))
// })

// gulp.task('flash:prod', function () {
//   return gulp.src(paths.vendor.components.flash)
//     .pipe(gulp.dest(paths.dirs.build))
// })

// gulp.task('xml:prod', function () {
//   return gulp.src(paths.vendor.components.xml)
//     .pipe(gulp.dest(paths.dirs.build))
// })

// gulp.task('scripts:prod', function () {
//   var vendorJsStream = gulp.src(paths.vendor.bower.js)
//   var vendorComponentsJsStream = gulp.src(paths.vendor.components.js)
//   var coffeeStream = gulp.src(paths.coffee)
//     .pipe(coffee({ bare: true }))
//     .pipe(ngAnnotate( {
//       remove: true,
//       add: true,
//       single_quotes: true
//     }))

//   return merge(coffeeStream, vendorComponentsJsStream, vendorJsStream)
//       .pipe(order([
//         'bower_components/jquery/dist/jquery.js',
//         'bower_components/lodash/dist/lodash.js',
//         'bower_components/angular/angular.js',
//         'bower_components/**/*.js',
//         'app/components/angularjs-jwplayer/vendor/jwplayer/jwplayer.js',
//         'app/components/**/vendor/**/*.js',
//         'app/**/app.js'
//       ], { base: '.'}))
//       .pipe(concat('all.min.js'))
//       .pipe(uglifyJs())
//       .pipe(gulp.dest(paths.dirs.build))
// })

// gulp.task('styles:prod', function() {
//   var cssStream = gulp.src(paths.vendor.bower.css)

//   var sassStream = gulp.src(paths.sass)
//     .pipe(sass())

//   var lessStream = gulp.src(paths.less)
//     .pipe(less())

//   return merge(cssStream, lessStream, sassStream)
//     .pipe(concat('all.min.css'))
//     .pipe(minifyCss({ keepSpecialComments: 0 }))
//     .pipe(gulp.dest(paths.dirs.build))
// })

// gulp.task('all:prod', gulp.parallel(
//   'html:prod', 'images:prod', 'json:prod',
//   'scripts:prod', 'styles:prod', 'fonts:prod', 'flash:prod', 'xml:prod'
// ))

// gulp.task('build:prod', gulp.series('clean', 'all:prod'))

// // Default task
// gulp.task('default', gulp.series('build', 'ws', 'watch'))